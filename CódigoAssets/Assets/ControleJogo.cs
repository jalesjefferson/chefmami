﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ControleJogo : MonoBehaviour
{
    #region Variaveis

    #region Variáveis CHEF
    [Header("Configuração CHEF - Jogador")]
    public int casaAtual = 1;
    public Sprite[] chefNormalSprites = new Sprite[4];
    public Sprite[] chefColisao = new Sprite[4];
    public GameObject[] chefGO = new GameObject[4];
    public bool chefPodeColidir = true;
    public bool podeJogar = false;
    private SpriteRenderer[] chefSprRender = new SpriteRenderer[4];
    Coroutine rotinaPodeColidir = null;
    Coroutine rotinaVoltarSprite = null;
    #endregion

    #region Variaveis COMIDAS
    [Header("Configuração COMIDA")]
    public int[] posicaoComida = new int[4];
    public int[] posDestino = new int[5];
    public bool[] subindo = new bool[4];
    public bool[] comidaPodeColidir = new bool[4];
    public float[] alturaComida = new float[4];
    public GameObject[] comidaGO = new GameObject[4];
    public ListaSpritesComida[] spritesComida = new ListaSpritesComida[4];
    private SpriteRenderer[] comidaSprite = new SpriteRenderer[4];
    public Coroutine[] rotinaComida = new Coroutine[4];
    #endregion

    #region Variáveis GATO
    [Header("Config GATO")]
    public bool tentandoPegar = false;
    public bool pegouSalsicha = false;
    public SpriteRenderer SprRenderGato = null;
    public Sprite[] spritesGato = new Sprite[2];
    public int gatoTempoFincar = 0; 
    public int gatoTempoSair = 0;
    Coroutine gatoFincar = null;
    #endregion

    #region Variáveis RATO
    [Header("Config RATO")]
    public GameObject[] objsRato = new GameObject[6];
    public GameObject[] missQtd = new GameObject[3];
    #endregion

    #region Variáveis CONTROLE JOGO
    [Header("Configuração JOGO")]
    public int qtdErros = 0;
    public int pontuacao = 0;
    public Text textoPonto = null;
    public GameObject missGO = null;
    public float velocidadeJogo = 1;
    public TiposIntro[] intro = new TiposIntro[6];
    public AudioSource AsPrincipal, sfx;
    public AudioClip musicaPricipal, somRato, somAcerto;
    #endregion

    #endregion

    void Awake()
    {
        GetSpriteRender();
    }

    void Start()
    {
        StartCoroutine(IniciarJogo());
    }

    void Update()
    {
        if (podeJogar)
        {
            ReceberInputMov();
            VerificarColisao();
        }

        if (tentandoPegar)
            VerificarPegouSalsicha();
    }

    #region Metodos para o CHEF

    private void ReceberInputMov()
    {
        if (Input.GetButtonDown("Fire2"))
            RealizarMovimento(1);
        else if (Input.GetButtonDown("Fire1"))
            RealizarMovimento(-1);
    }

    private void RealizarMovimento(int dir)
    {
        int antigaCasa = casaAtual;
        int casaDestino = casaAtual + dir;
        casaAtual = Mathf.Clamp(casaDestino, 0, 3);

        if (casaAtual == casaDestino)
        {
            chefSprRender[antigaCasa].sprite = chefNormalSprites[antigaCasa];
            chefGO[antigaCasa].SetActive(false);

            chefGO[casaAtual].SetActive(true);
            chefSprRender[casaAtual].sprite = chefNormalSprites[casaAtual];

            chefPodeColidir = false;
            if (rotinaPodeColidir != null)
                StopCoroutine(rotinaPodeColidir);
            rotinaPodeColidir = StartCoroutine(ChefPodeColidir());

            if (rotinaVoltarSprite != null)
                StopCoroutine(rotinaVoltarSprite);
        }
    }

    private IEnumerator ChefPodeColidir()
    {
        yield return new WaitForSeconds(0.02f);
        chefPodeColidir = true;
    }

    private void ColisaoChefComida(int i)
    {
        chefSprRender[i].sprite = chefColisao[i];
        rotinaVoltarSprite = StartCoroutine(VoltarSpriteNormal(i));
    }

    private IEnumerator VoltarSpriteNormal(int i)
    {
        yield return new WaitForSeconds(0.6f);
        chefSprRender[i].sprite = chefNormalSprites[i];
        rotinaVoltarSprite = null;
    }

    #endregion

    #region Métodos para COMIDA

    private IEnumerator IniciarRotinaMovimentoComida()
    {
        int qualTipo = UnityEngine.Random.Range(0, intro.Length);

        for (int i = 0; i < 4; i++)
        {
            rotinaComida[intro[qualTipo].sequencia[i]] = StartCoroutine(TempoMoverComida(intro[qualTipo].sequencia[i]));
            yield return new WaitForSeconds(0.3f);
        }
    }

    private IEnumerator TempoMoverComida(int qualComida)
    {
        while (true)
        {
            yield return new WaitForSeconds(velocidadeJogo);
            MoverComida(qualComida);
            TocarMusica();
        }
    }

    private IEnumerator ComidaPodeColidir(int qualComida)
    {
        yield return new WaitForSeconds(0.1f);
        comidaPodeColidir[qualComida] = true;
    }

    private void MoverComida(int qualComida)
    {
        Vector2 pos = comidaGO[qualComida].transform.position;
        if (subindo[qualComida])
        {
            posicaoComida[qualComida]--;
            if (posicaoComida[qualComida] == posDestino[qualComida])
                subindo[qualComida] = false;
            pos.y = alturaComida[posicaoComida[qualComida]];
            ConfigComidaVisual(qualComida, posicaoComida[qualComida]);
        }
        else
        {
            posicaoComida[qualComida]++;

            if (posicaoComida[qualComida] == 4)
                StartCoroutine(ComidaPodeColidir(qualComida));

            if (posicaoComida[qualComida] >= 5)
            {
                StopAllCoroutines();
                StartCoroutine(ConsequenciaErro(qualComida));
                return;
            }

            pos.y = alturaComida[posicaoComida[qualComida]];
            ConfigComidaVisual(qualComida, posicaoComida[qualComida]);
        }

    }

    private void ConfigComidaVisual(int qualComida, int posComida)
    {
        Vector2 pos = comidaGO[qualComida].transform.position;
        pos.y = alturaComida[posComida];
        comidaGO[qualComida].transform.position = pos;
        comidaSprite[qualComida].sprite = spritesComida[qualComida].spritesComida[posicaoComida[qualComida]];
    }

    #endregion

    #region Métodos Para o Gato

    private void GatoTempoSegurar()
    {
        gatoTempoFincar = UnityEngine.Random.Range(1, 15);
        gatoTempoSair = UnityEngine.Random.Range(1, 15);
        SprRenderGato.sprite = spritesGato[0];

    }

    private IEnumerator FincarGafo()
    {
        GatoTempoSegurar();
        yield return new WaitForSeconds(gatoTempoFincar);
        ConfigGato(true);
        yield return new WaitForSeconds(gatoTempoSair);
        ConfigGato(false);
    }

    private void ConfigGato(bool colocando)
    {
        tentandoPegar = colocando;
        SprRenderGato.sprite = spritesGato[(colocando ? 1 : 0)];
        if (!tentandoPegar)
        {
            if (pegouSalsicha)
            {
                pegouSalsicha = false;
                posicaoComida[0] = 3;
                ConfigComidaVisual(0, posicaoComida[0]);
                rotinaComida[0] = StartCoroutine(TempoMoverComida(0));
            }

            StartCoroutine(FincarGafo());
        }
    }
    
    private void VerificarPegouSalsicha()
    {
        if (tentandoPegar && posicaoComida[0] == 2 && !pegouSalsicha)
        {
            subindo[0] = false;
            pegouSalsicha = true;
            StopCoroutine(rotinaComida[0]);
            rotinaComida[0] = null;
        }
    }

    #endregion

    #region Métodos para o RATO

    IEnumerator AnimacaoRatoComendo(int valorErro)
    {
        comidaGO[valorErro].SetActive(false);
        valorErro++;
        objsRato[0].SetActive(false);
        objsRato[valorErro].SetActive(true);
        TocarFx(0);
        yield return new WaitForSeconds(1);

        for (int i = valorErro; i < 6; i++)
        {
            if (i != valorErro)
                objsRato[i-1].SetActive(false);
            objsRato[i].SetActive(true);
            TocarFx(0);
            if (i != 5)
                yield return new WaitForSeconds(0.2f);
        }
        qtdErros++;

        yield return new WaitForSeconds(1);

        if (qtdErros == 1)
            missGO.SetActive(true);

        missQtd[qtdErros-1].SetActive(true);

        yield return new WaitForSeconds(0.5f);

        objsRato[5].SetActive(false);
        objsRato[0].SetActive(true);
    }

    #endregion

    #region Métodos controle Som

    private void TocarMusica(){
        AsPrincipal.Play();
    }

    private void TocarFx(int qualMusica){
        switch(qualMusica){
            case 0: 
                sfx.clip = somRato;
                break;
            case 1: 
                sfx.clip = somAcerto;
                break;
        }
        sfx.Play();
    }
    #endregion

    #region Métodos Controle do jogo Geral

    private void VerificarColisao()
    {
        for (int i = 0; i < 4; i++)
        {
            if (comidaGO[i].activeInHierarchy && posicaoComida[i] == 4 && casaAtual == i && chefPodeColidir && comidaPodeColidir[i])
            {
                ComidaColidiu(i);
                ColisaoChefComida(i);
                AumentarPontuacao();
                TocarFx(1);
            }
        }
    }

    private void ComidaColidiu(int i)
    {
        subindo[i] = true;
        comidaPodeColidir[i] = false;
        posDestino[i] = UnityEngine.Random.Range(0, 3);
        posicaoComida[i] = 3;
        ConfigComidaVisual(i, posicaoComida[i]);
    }

    private void GetSpriteRender()
    {
        for (int i = 0; i < 4; i++)
        {
           chefSprRender[i] = chefGO[i].GetComponent<SpriteRenderer>();
           comidaSprite[i] = comidaGO[i].GetComponent<SpriteRenderer>();
        }
    }

    private void AumentarPontuacao()
    {
        pontuacao++;
        textoPonto.text = pontuacao.ToString();

        switch (pontuacao)
        {
            case (200):
                RecuperarVida();
                break;
            case (500):
                RecuperarVida();
                break;
        }

        AumentarVelocidadeJogo();
    }

    private void RecuperarVida()
    {
        qtdErros = 0;
        missGO.SetActive(false);
        foreach (GameObject go in missQtd)
        {
            go.SetActive(false);
        }
    }

    private void AumentarVelocidadeJogo()
    {
        velocidadeJogo -= 0.002f;
        velocidadeJogo = Mathf.Clamp(velocidadeJogo, 0.5f, velocidadeJogo);
    }

    private void ResetarJogo()
    {
        for (int i = 0; i < 4; i++)
        {
            chefSprRender[i].sprite = chefNormalSprites[i];
            chefGO[i].SetActive(true);
            posicaoComida[i] = 4;
            ConfigComidaVisual(i, 4);
            rotinaComida[i] = null;
        }

        StopCoroutine(gatoFincar);
        StartCoroutine(IniciarJogo());
    }

    // Simulador de começo de jogo
    IEnumerator ConsequenciaErro(int qualComida)
    {
        podeJogar = false;
        casaAtual = 1;

        yield return StartCoroutine(AnimacaoRatoComendo(qualComida));

        if (qtdErros < 3)
            ResetarJogo();
        else
            this.enabled = false;
    }

    IEnumerator IniciarJogo()
    {
        ReconfiguracaoGeral();
        yield return new WaitForSeconds(0.5f);
        SimularInicioJogo();
        yield return new WaitForSeconds(0.5f);
        JogadorPodeJogar();
        gatoFincar = StartCoroutine(FincarGafo());
    }

    private void ReconfiguracaoGeral()
    {
        for (int i = 0; i < 4; i++)
        {
            comidaGO[i].SetActive(true);
        }

        tentandoPegar = false;
        pegouSalsicha = false;
        SprRenderGato.sprite = spritesGato[0];
    }

    private void SimularInicioJogo()
    {
        for (int i = 0; i < 4; i++)
        {
            ComidaColidiu(i);
            ColisaoChefComida(i);
        }

        StartCoroutine(IniciarRotinaMovimentoComida());
        TocarFx(1);

    }

    private void JogadorPodeJogar()
    {
        for (int i = 0; i < 4; i++)
        {
            if (i != casaAtual)
                chefGO[i].SetActive(false);
        }

        podeJogar = true;
        chefPodeColidir = true;
    }

    #endregion
}
[Serializable]
public class ListaSpritesComida
{
    public string name;
    public Sprite[] spritesComida = new Sprite[5];
}