﻿using UnityEngine;

[CreateAssetMenu(fileName = "Tipos de comeco", menuName = "Variantes de  inicio")]
public class TiposIntro : ScriptableObject
{
    public int[] sequencia = new int[4];

}
